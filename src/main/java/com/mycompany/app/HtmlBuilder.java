package com.mycompany.app;

import org.json.simple.JSONObject;

import java.util.ArrayList;

public class HtmlBuilder {
    public String coursesResponse(ArrayList<Course> courses){
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Courses</title>\n" +
                "    <style>\n" +
                "        table{\n" +
                "            width: 100%;\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <table>\n" +
                "        <tr>\n" +
                "            <th>Code</th>\n" +
                "            <th>Class Code</th> \n" +
                "            <th>Name</th>\n" +
                "            <th>Units</th>\n" +
                "            <th>Capacity</th>\n" +
                "            <th>Type</th>\n" +
                "            <th>Days</th>\n" +
                "            <th>Time</th>\n" +
                "            <th>Exam Start</th>\n" +
                "            <th>Exam End</th>\n" +
                "            <th>Prerequisites</th>\n" +
                "            <th>Links</th>\n" +
                "        </tr>\n";
        for (Course course : courses){
            //JSONObject classTime = course.getClassTime();
            //ArrayList<String> days = classTime.get("days");
            //String time = (String) classTime.get("time");
            response = response + "        <tr>\n" +
                    "            <td>" + course.getCode() + "</td>\n" +
                    "            <td>" + course.getClassCode() + "</td> \n" +
                    "            <td>" + course.getName() + "</td>\n" +
                    "            <td>" + course.getUnits() + "</td>\n" +
                    "            <td>" + course.getCapacity() + "</td>\n" +
                    "            <td>" + course.getType() + "</td>\n" +
                    "            <td>" + ((ArrayList<String>) course.getClassTime().get("days")).toString() + "</td>\n" +
                    "            <td>" + (String) course.getClassTime().get("time") + "</td>\n" +
                    "            <td>" + (String) course.getExamTime().get("start") + "</td>\n" +
                    "            <td>" + (String) course.getExamTime().get("end") + "</td>\n" +
                    "            <td>" + course.getPrerequisites() + "</td>\n" +
                    "            <td><a href=\"/course/" + course.getCode() + "/" + course.getClassCode() + "\"" + ">Link</a></td>\n" +
                    "        </tr>\n";
        }
        response = response + "    </table>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String courseResponse(Course course){
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Course</title>\n" +
                "    <style>\n" +
                "        li {\n" +
                "        \tpadding: 5px\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <ul>\n" +
                "        <li id=\"code\">Code: " + course.getCode() + "</li>\n" +
                "        <li id=\"class_code\">Class Code: " + course.getClassCode() + "</li>\n" +
                "        <li id=\"units\">units: " + course.getUnits() + "</li>\n" +
                "        <li id=\"days\">Days: " + course.getClassTime().get("days") + "</li>\n" +
                "        <li id=\"time\">Time: " + course.getClassTime().get("time") + "</li>\n" +
                "        <form action=\"\" method=\"POST\" >\n" +
                "            <label>Student ID:</label>\n" +
                "            <input type=\"text\" name=\"std_id\" value=\"\"/>\n" +
                "            <button type=\"submit\">Add</button>\n" +
                "        </form>\n" +
                "    </ul>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String profileResponse(Student student){
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Profile</title>\n" +
                "    <style>\n" +
                "        li {\n" +
                "        \tpadding: 5px\n" +
                "        }\n" +
                "        table{\n" +
                "            width: 10%;\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <ul>\n" +
                "        <li id=\"std_id\">Student Id: " + student.getStudentId() + "</li>\n" +
                "        <li id=\"first_name\">First Name: " + student.getName() + "</li>\n" +
                "        <li id=\"last_name\">Last Name: " + student.getSecondName() + "</li>\n" +
                "        <li id=\"birthdate\">Birthdate: " + student.getBirthDate() + "</li>\n" +
                "        <li id=\"gpa\">GPA: " + student.getGPA() + "</li>\n" +
                "        <li id=\"tpu\">Total Passed Units: " + student.getNumOfPassedUnits() + "</li>\n" +
                "    </ul>\n" +
                "    <table>\n" +
                "        <tr>\n" +
                "            <th>Code</th>\n" +
                "            <th>Grade</th> \n" +
                "        </tr>\n";
        ArrayList<Grade> passedCourses = student.getPassedCourses();
        for (Grade passedCourse : passedCourses){
            response = response + "        <tr>\n" +
                    "            <td>" + passedCourse.getCode() + "</td>\n" +
                    "            <td>" + passedCourse.getGrade() + "</td> \n" +
                    "        </tr>\n";
        }
        response = response + "    </table>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String changePlanResponse(Student student){
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Courses</title>\n" +
                "    <style>\n" +
                "        table{\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <table>\n" +
                "        <tr>\n" +
                "            <th>Code</th>\n" +
                "            <th>Class Code</th> \n" +
                "            <th>Name</th>\n" +
                "            <th>Units</th>\n" +
                "            <th></th>\n" +
                "        </tr>\n";

        ArrayList<Course> weeklySchedule = student.getWeeklyScheduleCourses();
        for (Course course : weeklySchedule){
            response = response + "<tr>\n" +
                    "            <td>" + course.getCode() + "</td>\n" +
                    "            <td>" + course.getClassCode() + "</td> \n" +
                    "            <td>" + course.getName() + "</td>\n" +
                    "            <td>" + course.getUnits() + "</td>\n" +
                    "            <td>        \n" +
                    "                <form action=\"\" method=\"POST\" >\n" +
                    "                    <input id=\"form_course_code\" type=\"hidden\" name=\"course_code\" value=\"" + course.getCode() + "\">\n" +
                    "                    <input id=\"form_class_code\" type=\"hidden\" name=\"class_code\" value=\"" + course.getClassCode() + "\">\n" +
                    "                    <button type=\"submit\">Remove</button>\n" +
                    "                </form>\n" +
                    "            </td>\n" +
                    "        </tr>\n";
        }
        response = response + "    </table>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String planResponse(Student student){
        ArrayList<Course> weeklySchedule = student.getWeeklyScheduleCourses();
        String sat0 = ""; String sat1 = ""; String sat2 = ""; String sat3 = ""; String sat4 = "";
        String sun0 = ""; String sun1 = ""; String sun2 = ""; String sun3 = ""; String sun4 = "";
        String mon0 = ""; String mon1 = ""; String mon2 = ""; String mon3 = ""; String mon4 = "";
        String tue0 = ""; String tue1 = ""; String tue2 = ""; String tue3 = ""; String tue4 = "";
        String wed0 = ""; String wed1 = ""; String wed2 = ""; String wed3 = ""; String wed4 = "";
        for (Course course : weeklySchedule){
            String name = course.getName();
            JSONObject classTime = course.getClassTime();
            String time =  (String) classTime.get("time");
            ArrayList<String> days = (ArrayList<String>) classTime.get("days");
            if (time.equals("7:30-9:00")){
                if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                    sat0 = name;
                else if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                    sun0 = name;
                else if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                    mon0 = name;
                else if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                    tue0 = name;
                else if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                    wed0 = name;
            }
            else if (time.equals("9:00-10:30")){
                if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                    sat1 = name;
                else if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                    sun1 = name;
                else if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                    mon1 = name;
                else if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                    tue1 = name;
                else if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                    wed1 = name;
            }
            else if (time.equals("10:30-12:00")){
                if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                    sat2 = name;
                else if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                    sun2 = name;
                else if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                    mon2 = name;
                else if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                    tue2 = name;
                else if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                    wed2 = name;
            }
            else if (time.equals("14:00-15:30")){
                if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                    sat3 = name;
                else if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                    sun3 = name;
                else if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                    mon3 = name;
                else if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                    tue3 = name;
                else if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                    wed3 = name;
            }
            else if (time.equals("16:00-17:30")){
                if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                    sat4 = name;
                else if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                    sun4 = name;
                else if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                    mon4 = name;
                else if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                    tue4 = name;
                else if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                    wed4 = name;
            }

        }

        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Plan</title>\n" +
                "    <style>\n" +
                "        table{\n" +
                "            width: 100%;\n" +
                "            text-align: center;\n" +
                "            \n" +
                "        }\n" +
                "        table, th, td{\n" +
                "            border: 1px solid black;\n" +
                "            border-collapse: collapse;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <table>\n" +
                "        <tr>\n" +
                "            <th></th>\n" +
                "            <th>7:30-9:00</th>\n" +
                "            <th>9:00-10:30</th>\n" +
                "            <th>10:30-12:00</th>\n" +
                "            <th>14:00-15:30</th>\n" +
                "            <th>16:00-17:30</th>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>Saturday</td>\n" +
                "            <td>" + sat0 + "</td>\n" +
                "            <td>" + sat1 + "</td>\n" +
                "            <td>" + sat2 + "</td>\n" +
                "            <td>" + sat3 + "</td>\n" +
                "            <td>" + sat4 + "</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>Sunday</td>\n" +
                "            <td>" + sun0 + "</td>\n" +
                "            <td>" + sun1 + "</td>\n" +
                "            <td>" + sun2 + "</td>\n" +
                "            <td>" + sun3 + "</td>\n" +
                "            <td>" + sun4 + "</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>Monday</td>\n" +
                "            <td>" + mon0 + "</td>\n" +
                "            <td>" + mon1 + "</td>\n" +
                "            <td>" + mon2 + "</td>\n" +
                "            <td>" + mon3 + "</td>\n" +
                "            <td>" + mon4 + "</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>Tuesday</td>\n" +
                "            <td>" + tue0 + "</td>\n" +
                "            <td>" + tue1 + "</td>\n" +
                "            <td>" + tue2 + "</td>\n" +
                "            <td>" + tue3 + "</td>\n" +
                "            <td>" + tue4 + "</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>Wednesday</td>\n" +
                "            <td>" + wed0 + "</td>\n" +
                "            <td>" + wed1 + "</td>\n" +
                "            <td>" + wed2 + "</td>\n" +
                "            <td>" + wed3 + "</td>\n" +
                "            <td>" + wed4 + "</td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String notFoundResponse(){
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>404 Error</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <h1>404<br>Page Not Found</h1>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String submitResponse(Student student) {
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Submit</title>\n" +
                "    <style>\n" +
                "        li {\n" +
                "        \tpadding: 5px\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <ul>\n" +
                "        <li id=\"code\">Student Id:" + student.getStudentId() + "</li>\n" +
                "        <li id=\"units\">Total Units:" + student.getWeeklySchedule().getTotalUnits() + "</li>\n" +
                "        <form action=\"\" method=\"POST\" >\n" +
                "            <button type=\"submit\">submit</button>\n" +
                "        </form>\n" +
                "    </ul>\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String submitOkResponse() {
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Submit OK</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    Your request submited successfuly\n" +
                "</body>\n" +
                "</html>";
        return response;
    }

    public String submitFailedResponse() {
        String response = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Submit Failed</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    Your request failed\n" +
                "</body>\n" +
                "</html>";
        return response;
    }
}
