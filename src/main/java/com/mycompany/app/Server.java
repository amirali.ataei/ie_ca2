package com.mycompany.app;

import com.mycompany.app.pages.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import io.javalin.Javalin;
import org.reflections.Reflections;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Handler;

import static com.mycompany.app.Tools.getCourse;
import static com.mycompany.app.Tools.getStudent;
import static io.javalin.apibuilder.ApiBuilder.*;

public class Server {
    Set<Class<?>> pages;
    public void startServer() throws Exception {
        Javalin server = Javalin.create().start(8080);
        server.get("courses", ctx -> {
            String response = new CoursesPage().get();
            ctx.html(response);
        });

        server.get("profile/:id", ctx -> {
            String response = new ProfilePage().get(ctx.pathParam("id"));
            ctx.html(response);
        });

        server.routes(() -> {
             path("course", () -> {
                 get(":course_id/:class_code", ctx -> {
                     String code = ctx.pathParam("course_id");
                     String classCode = ctx.pathParam("class_code");
                     String response = new CoursePage().get(code, classCode);
                     ctx.html(response);
                 });
                 post(":course_id/:class_code", ctx -> {
                     String code = ctx.pathParam("course_id");
                     String classCode = ctx.pathParam("class_code");
                     Course course = getCourse(code, classCode);
                     Student student = getStudent(ctx.formParam("std_id"));
                     student.addToWeeklySchedule(course);
                 });
            });
        });

        server.routes(() -> {
            path("change_plan", () -> {
                get(":id", ctx -> {
                    String studentId = ctx.pathParam("id");
                    String response = new ChangePlanPage().get(studentId);
                    ctx.html(response);
                });
                post(":id", context -> {
                    String studentId = context.pathParam("id");
                    Student student = getStudent(studentId);
                    String code = context.formParam("course_code");
                    String group = context.formParam("class_code");
                    Course course = getCourse(code, group);
                    student.removeFromWeeklySchedule(course);
                });
            });
        });

        server.get("plan/:id", context -> {
            String studentId = context.pathParam("id");
            String response = new PlanPage().get(studentId);
            context.html(response);
        });

        server.routes(() -> {
            path("submit", () -> {
                get(":id", context -> {
                    String studentId = context.pathParam("id");
                    String response = new SubmitPage().get(studentId);
                    context.html(response);
                });
                post(":id", context -> {
                    String studentId = context.pathParam("id");
                    Student student = getStudent(studentId);
                    int units = student.getWeeklySchedule().getTotalUnits();
                    String response = "";
                    if(units >= 12 && units <= 20)
                        response = new SubmitOkPage().get();
                    else
                        response = new SubmitFailedPage().get();
                    context.html(response);
                });
            });

        });
    }
}
