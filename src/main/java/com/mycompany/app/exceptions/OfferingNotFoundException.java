package com.mycompany.app.exceptions;

public class OfferingNotFoundException extends Exception{
    public String toString(){
        return "OfferingNotFound";
    }
}
