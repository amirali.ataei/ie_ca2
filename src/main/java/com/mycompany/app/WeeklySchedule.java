package com.mycompany.app;

import com.mycompany.app.exceptions.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;
import java.util.Iterator;

public class WeeklySchedule {
    private ArrayList<Course> courses;
    private int totalUnits;

    public WeeklySchedule() {
        courses = new ArrayList<>();
        this.totalUnits=0;
    }

    public int getTotalUnits() {
        return totalUnits;
    }

    private void checkMinUnits() throws MinimumUnitsException {
        if(this.totalUnits<12)
            throw new MinimumUnitsException();
    }
    private void checkMaxUnits() throws MaximumUnitsException {
        if(this.totalUnits>20)
            throw new MaximumUnitsException();
    }
    private void checkClassTimeCollision() throws ClassTimeCollisionException {

        for(Course course: courses)
        {
            JSONObject classTime = course.getClassTime();
            ArrayList<String> days = (ArrayList<String>) classTime.get("days");
            String time = (String) classTime.get("time");
            for(Course _course: courses)
            {
                if(course.getCode().equals(_course.getCode()))
                    continue;
                JSONObject _classTime = _course.getClassTime();
                ArrayList<String> _days = (ArrayList<String>) _classTime.get("days");
                String _time = (String) _classTime.get("time");

                if(days.get(0).equals(_days.get(0)) || days.get(0).equals(_days.get(1)) || days.get(1).equals(_days.get(1)) || days.get(1).equals(_days.get(0))){
                    if(time.equals(_time))
                        throw new ClassTimeCollisionException(course.getCode(), _course.getCode());
                }
            }
        }
    }
    private void checkExamTimeCollision() throws ExamTimeCollisionException {

        for(Course course : courses)
        {
            JSONObject examTime = course.getExamTime();
            String start = (String) examTime.get("start");
            String end = (String) examTime.get("end");

            for(Course _course : courses)
            {
                if(course.getCode().equals(_course.getCode()))
                    continue;
                JSONObject _examTime = _course.getExamTime();
                String _start = (String) _examTime.get("start");
                String _end = (String) _examTime.get("end");

                if(start.equals(_start) && end.equals(_end)){
                    throw new ExamTimeCollisionException(course.getCode(), _course.getCode());
                }
            }
        }
    }
    private void checkCapacity() throws CapacityException {
        for(Course course : courses)
        {
            if(course.getOccupiedCapacity() > course.getCapacity())
                throw new CapacityException();
        }
    }

    private void check() throws Exception{
        this.checkMinUnits();
        this.checkMaxUnits();
//        this.checkClassTimeCollision();
//        this.checkExamTimeCollision();
//        this.checkCapacity();
    }

    public void aTWS(Course course){
        this.courses.add(course);
        this.totalUnits=this.totalUnits+course.getUnits();
    }
    public void rFWS(Course course) throws OfferingNotFoundException {
        boolean courseFound = false;
        for(Course _course : courses)
        {
            if(_course.getCode().equals(course.getCode()))
            {
                courses.remove(_course);
                this.totalUnits=this.totalUnits - course.getUnits();
                return ;
            }
        }
        throw new OfferingNotFoundException();
    }

    public ArrayList<Course> gWS() {
        return courses;
    }


    public void fWS() throws Exception{
        this.check();
        for(Course course : courses)
            course.finalizeOffer();
    }
}
