package com.mycompany.app;

public class Grade {
    private String code;
    private double grade;
    private int units;

    public Grade(String _code, int _grade) {
        code = _code;
        grade = _grade;
    }

    public String getCode() {
        return code;
    }

    public double getGrade() {
        return grade;
    }

    public int getUnits() {
        return units;
    }
}
