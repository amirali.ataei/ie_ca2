package com.mycompany.app.pages;

import com.mycompany.app.GetMethod;
import com.mycompany.app.HtmlBuilder;
import com.mycompany.app.Student;
import com.mycompany.app.WebPage;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.StringTokenizer;

import static com.mycompany.app.Tools.getStudent;

@WebPage(resource = "submit_failed")
public class SubmitFailedPage {
    private HtmlBuilder hb;

    public SubmitFailedPage(){
        hb = new HtmlBuilder();
    }

    @GetMethod
    public String get() throws IOException {
        String response;
        try {
            response = this.hb.submitFailedResponse();
        } catch (Exception ex){
            response = this.hb.notFoundResponse();
        }
        return response;
    }
}