package com.mycompany.app.pages;

import com.mycompany.app.Course;
import com.mycompany.app.GetMethod;
import com.mycompany.app.HtmlBuilder;
import com.mycompany.app.WebPage;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static com.mycompany.app.Tools.getCourse;

@WebPage(resource = "course")
public class CoursePage {
    private HtmlBuilder hb;

    public CoursePage(){
        hb = new HtmlBuilder();
    }

    @GetMethod
    public String get(String code, String group) throws IOException{
        Course course;
        String response;
        try {
            course = getCourse(code, group);
            response = this.hb.courseResponse(course);
        } catch (Exception ex) {
            response = this.hb.notFoundResponse();
        }
        return response;
    }
}
