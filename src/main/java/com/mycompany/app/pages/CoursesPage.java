package com.mycompany.app.pages;

import com.mycompany.app.*;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.mycompany.app.Tools.getCourses;

@WebPage(resource = "courses")
public class CoursesPage {
    private HtmlBuilder hb;

    public CoursesPage(){
        hb = new HtmlBuilder();
    }

    @GetMethod
    public String get() throws IOException{
        ArrayList<Course> courses = new ArrayList<>();
        try {
            courses = App.courses;
        } catch (Exception ex) {}
        String response = hb.coursesResponse(courses);
        return response;
    }
}
