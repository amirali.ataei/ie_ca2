package com.mycompany.app.pages;

import com.mycompany.app.GetMethod;
import com.mycompany.app.HtmlBuilder;
import com.mycompany.app.Student;
import com.mycompany.app.WebPage;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static com.mycompany.app.Tools.getStudent;

@WebPage(resource = "plan")
public class PlanPage {
    private HtmlBuilder hb;

    public PlanPage(){
        hb = new HtmlBuilder();
    }

    @GetMethod
    public String get(String studentId) throws IOException{
        Student student;
        String response;
        try {
            student = getStudent(studentId);
            response = this.hb.planResponse(student);
        } catch (Exception ex){
            response = this.hb.notFoundResponse();
        }
        return response;
    }
}
