package com.mycompany.app;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static com.mycompany.app.Tools.*;

public class App {
    public static ArrayList<Student> students;
    public static ArrayList<Course> courses;

    public static void main(String[] args) throws Exception {
        courses = getCourses();
        students = getStudents();

        for(Student student:students){
            System.out.println(student.getStudentId());
        }

        Server server = new Server();
        server.startServer();
    }
}
