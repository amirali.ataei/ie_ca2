package com.mycompany.app;

import com.google.gson.Gson;
import com.mycompany.app.exceptions.OfferingNotFoundException;
import com.mycompany.app.exceptions.StudentNotFoundException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Tools {
    public static ArrayList<Course> getCourses() throws Exception {
        URL url = new URL("http://138.197.181.131:5000/api/courses");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        ArrayList<Course> courses = new ArrayList<Course>();

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Object obj = JSONValue.parse(inputLine);
            JSONArray array = (JSONArray)obj;
            for(Object object : array)
            {
                JSONObject jsonObject = (JSONObject) object;
                Gson gson = new Gson();
                courses.add(gson.fromJson(jsonObject.toJSONString(), Course.class));
            }
        }
        in.close();
        con.disconnect();
        return courses;
    }

    public static ArrayList<Student> getStudents() throws Exception {
        URL url = new URL("http://138.197.181.131:5000/api/students");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        ArrayList<Student> students = new ArrayList<Student>();

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Object obj = JSONValue.parse(inputLine);
            JSONArray array = (JSONArray) obj;
            for(Object object : array)
            {
                JSONObject jsonObject = (JSONObject) object;
                Gson gson = new Gson();
                students.add(gson.fromJson(jsonObject.toJSONString(), Student.class));
            }
        }
        in.close();
        con.disconnect();
        return students;
    }

    public static ArrayList<Grade> getGrades(String student_id) throws Exception {
        URL url = new URL("http://138.197.181.131:5000/api/grades/" + student_id);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        ArrayList<Grade> grades = new ArrayList<Grade>();

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Object obj = JSONValue.parse(inputLine);
            JSONArray array = (JSONArray) obj;
            for(Object object : array)
            {
                JSONObject jsonObject = (JSONObject) object;
                Gson gson = new Gson();
                grades.add(gson.fromJson(jsonObject.toJSONString(), Grade.class));
            }
        }
        in.close();
        con.disconnect();
        return grades;
    }

    public static Course getCourse(String code, String classCode) throws Exception {
        for(Course course : App.courses) {
            if(course.getCode().equals(code) && course.getClassCode().equals(classCode))
                return course;
        }
        throw new OfferingNotFoundException();
    }

    public static Student getStudent(String studentId) throws Exception {
        for(Student student : App.students) {
            if(student.getStudentId().equals(studentId))
                return student;
        }
        throw new StudentNotFoundException();
    }
}
