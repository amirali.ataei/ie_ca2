package com.mycompany.app;

import com.google.gson.Gson;
import com.mycompany.app.exceptions.ClassTimeCollisionException;
import com.mycompany.app.exceptions.MinimumUnitsException;
import org.json.simple.JSONObject;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class AppTest {

    Gson gson = new Gson();

    String json = "{\"code\": \"8101001\", \"classCode\": \"01\", \"name\": \"Advanced Programming\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 45, \"prerequisites\": [\"8101013\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"10:30-12:00\"}, \"examTime\": {\"start\": \"2021-06-21T14:00:00\", \"end\": \"2021-06-21T17:00:00\"}}";

    Course course = gson.fromJson(json, Course.class);
    Course course1 = gson.fromJson("{\"code\": \"8101002\", \"classCode\": \"01\", \"name\": \"Algorithm Design 1\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 45, \"prerequisites\": [\"8101009\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"7:30-9:00\"}, \"examTime\": {\"start\": \"2021-06-12T14:00:00\", \"end\": \"2021-06-12T17:00:00\"}}", Course.class);
    Course course2 = gson.fromJson("{\"code\": \"8101003\", \"classCode\": \"01\", \"name\": \"Artificial Intelligence\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 90, \"prerequisites\": [\"8101002\"], \"classTime\": {\"days\": [\"Saturday\", \"Monday\"], \"time\": \"16:00-17:30\"}, \"examTime\": {\"start\": \"2021-06-22T08:30:00\", \"end\": \"2021-06-22T11:30:00\"}}", Course.class);
    Course course3 = gson.fromJson("{\"code\": \"8101004\", \"classCode\": \"01\", \"name\": \"Calculus 1\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 90, \"prerequisites\": [], \"classTime\": {\"days\": [\"Saturday\", \"Monday\"], \"time\": \"14:00-15:30\"}, \"examTime\": {\"start\": \"2021-06-16T14:00:00\", \"end\": \"2021-06-16T17:00:00\"}}", Course.class);
    Course course4 = gson.fromJson("{\"code\": \"8101005\", \"classCode\": \"01\", \"name\": \"Calculus 2\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 45, \"prerequisites\": [\"8101004\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"10:30-12:00\"}, \"examTime\": {\"start\": \"2021-06-04T14:00:00\", \"end\": \"2021-06-04T17:00:00\"}}", Course.class);
    Course course5 = gson.fromJson("{\"code\": \"8101006\", \"classCode\": \"01\", \"name\": \"Computer Aided Digital System Design\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 60, \"prerequisites\": [\"8101007\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"7:30-9:00\"}, \"examTime\": {\"start\": \"2021-06-19T14:00:00\", \"end\": \"2021-06-19T17:00:00\"}}", Course.class);
    Course course6 = gson.fromJson("{\"code\": \"8101007\", \"classCode\": \"01\", \"name\": \"Computer Architecture\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 60, \"prerequisites\": [\"8101014\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"10:30-12:00\"}, \"examTime\": {\"start\": \"2021-06-17T08:30:00\", \"end\": \"2021-06-17T11:30:00\"}}", Course.class);

    Student student = gson.fromJson("{\"id\": \"810196285\", \"name\": \"Sina\", \"secondName\": \"Mohammadian\", \"birthDate\": \"1378/08/04\"}", Student.class);

    @Test
    public void testCourseClass() {
        assertEquals("8101001",course.getCode());
        assertEquals("Advanced Programming",course.getName());
        assertEquals("sample",course.getInstructor());
        assertEquals(3,course.getUnits());
        assertEquals(45,course.getCapacity());
        assertEquals(0,course.getOccupiedCapacity());
        JSONObject classTime = course.getClassTime();
        ArrayList<String> days = (ArrayList<String>) classTime.get("days");
        String time = (String) classTime.get("time");
        assertEquals("Sunday",days.get(0));
        assertEquals("Tuesday",days.get(1));
        assertEquals("10:30-12:00",time);
        JSONObject examTime = course.getExamTime();
        String start = (String) examTime.get("start");
        String end = (String) examTime.get("end");
        assertEquals("2021-06-21T14:00:00",start);
        assertEquals("2021-06-21T17:00:00",end);
        ArrayList<String> prerequisites = course.getPrerequisites();
        assertEquals("8101013",prerequisites.get(0));
        course.addToStudents("810197633");
        assertEquals(1,course.getOccupiedCapacity());
    }

    @Test
    public void testWeeklyScheduleClass() throws Exception {
        WeeklySchedule ws = new WeeklySchedule();
        course3.addToStudents("810197633");
        ws.aTWS(course1);
        ws.aTWS(course2);
        ws.aTWS(course3);
        ws.aTWS(course4);
        ws.aTWS(course5);
        ws.rFWS(course2);
//        ws.rFWS(course1);
//        ws.rFWS(course3);
//        ws.aTWS(course6);
//        ws.rFWS(course4);
        ws.fWS();
        ArrayList<Course> courses = ws.gWS();
        assertEquals(course1.getCode(), courses.get(0).getCode());
        assertEquals(course3.getCode(), courses.get(1).getCode());
    }



    @Test(expected = MinimumUnitsException.class)
    public void testStudentClass() throws Exception {
        String studentId = student.getStudentId();
        assertEquals("810196285", studentId);
        course3.addToStudents("810197633");
        student.addToWeeklySchedule(course1);
        student.addToWeeklySchedule(course2);
        student.addToWeeklySchedule(course3);
        student.addToWeeklySchedule(course4);
        student.addToWeeklySchedule(course5);
        student.removeFromWeeklySchedule(course2);
        student.removeFromWeeklySchedule(course1);
        student.removeFromWeeklySchedule(course3);
        student.addToWeeklySchedule(course6);
        student.removeFromWeeklySchedule(course4);
        student.finalizeWeeklySchedule();
        ArrayList<Course> courses = student.getWeeklyScheduleCourses();
        assertEquals(course5.getCode(), courses.get(0).getCode());
        assertEquals(course6.getCode(), courses.get(1).getCode());
    }

}
